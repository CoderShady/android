package com.example.a3buttons;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button firstbutton;
    private Button thirdbutton;
    private Button secondbutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         firstbutton = (Button) findViewById(R.id.button1);
        secondbutton = (Button)findViewById(R.id.button2);
        thirdbutton = (Button)findViewById(R.id.button3);
        firstbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {
                Intent intent = new Intent(MainActivity.this, button1.class);
                startActivity(intent);
            }
        });
        secondbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {
                Intent intent = new Intent(MainActivity.this, button2.class);
                startActivity(intent);
            }         });
        thirdbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {
                Intent intent = new Intent(MainActivity.this, button3.class);
                startActivity(intent);
            }
        });
    }
}