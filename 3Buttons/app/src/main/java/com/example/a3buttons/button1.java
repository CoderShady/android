package com.example.a3buttons;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class button1 extends AppCompatActivity {
    private Button exit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button1);
        exit = (Button)findViewById(R.id.close);
        exit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)             {

                Intent intent = new Intent(button1.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}